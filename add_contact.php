<?php
session_start();

if (!isset($_SESSION['user_email'])) {

    header("Location: login.php");
    exit();
}

if (isset($_SESSION['user_email'])) {

    echo '<a href="logout.php">Logout</a>';
}

include 'db_config.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $name = $_POST['name'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];

    $user_id = $_SESSION['user_id'];

    $query = "INSERT INTO contacts (user_id, name, company, email, number) VALUES ('$user_id', '$name', '$company', '$email', '$phone')";
    $result = $conn->query($query);

    if ($result) {
        header("Location: contacts.php");
        exit();
    } else {
        echo "Error adding contact: " . $conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
    <h2>Add Contact</h2>

    <form id="addContactForm" method="POST">
        <label for="name">Name:</label>
        <input type="text" name="name" required><br>

        <label for="company">Company:</label>
        <input type="text" name="company" required><br>

        <label for="email">Email:</label>
        <input type="email" name="email" required><br>

        <label for="phone">Phone:</label>
        <input type="text" name="phone" required><br>

        <input type="submit" value="Submit">
    </form>
</body>
</html>

<script>
    $(document).ready(function () {
    });
</script>

<style>
    body {
        font-family: Arial, sans-serif;
        margin: 20px;
    }

    h2 {
        color: #333;
    }

    #addContactForm {
        width: 50%;
        margin-top: 20px;
    }

    #addContactForm label {
        display: block;
        margin-bottom: 5px;
    }

    #addContactForm input {
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
</style>
