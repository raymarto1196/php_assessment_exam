
<?php
session_start();

if (!isset($_SESSION['user_email'])) {

    header("Location: login.php");
    exit();
}

if (isset($_SESSION['user_email'])) {

    echo '<a href="logout.php">Logout</a>';
}

include 'db_config.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id'])) {

    if (!isset($_SESSION['user_email'])) {
        echo "Unauthorized access";
        exit();
    }

    $contactId = $_POST['id'];

    $contactId = filter_var($contactId, FILTER_VALIDATE_INT);

    if ($contactId === false) {
        echo "Invalid contact ID";
        exit();
    }

    $userId = $_SESSION['user_id'];

    $query = "DELETE FROM contacts WHERE user_id = $userId AND id = $contactId";
    $result = $conn->query($query);

    if ($result) {
        echo "Contact deleted successfully!";
    } else {
        echo "Error deleting contact: " . $conn->error;
    }
} else {
    echo "Invalid request";
}

?>