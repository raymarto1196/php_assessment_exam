<?php
include 'db_config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $email = $_POST["email"];
    $password = $_POST["password"];

    $query = "SELECT * FROM users WHERE email = '$email'";
    $result = $conn->query($query);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $hashedPassword = $row['password'];

        if (password_verify($password, $hashedPassword)) {

            session_start();
            $_SESSION['user_email'] = $email;
            $_SESSION['user_id'] = $row['id'];  
            echo "success";
        } else {
            echo "failure";
        }
    }
}

$conn->close();
?>
