<!DOCTYPE html>
<html lang="en">
<body>
    <h2>Thank You!</h2>

    <button id="continueButton" onclick="redirectToContacts()">Continue to Contacts</button>
</body>
</html>

<script>
    function redirectToContacts() {
        window.location.href = "contacts.php";
    }
</script>

<style>
    body {
        font-family: Arial, sans-serif;
        text-align: center;
        margin: 50px;
    }

    h2 {
        color: #27ae60;
    }

    p {
        color: #333;
    }

    #continueButton {
        padding: 10px 20px;
        background-color: #3498db;
        color: #fff;
        text-decoration: none;
        border: none;
        cursor: pointer;
        font-size: 16px;
        margin-top: 20px;
    }
</style>