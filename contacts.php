<?php
session_start();

if (!isset($_SESSION['user_email'])) {

    header("Location: login.php");
    exit();
}

if (isset($_SESSION['user_email'])) {

    echo '<a href="logout.php">Logout</a>';
}

include 'db_config.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['search'])) {

    $user_id = $_SESSION['user_id'];
    $search = $_GET['search'];

    $query = "SELECT * FROM contacts WHERE user_id = $user_id AND (name LIKE '%$search%' OR company LIKE '%$search%' OR number LIKE '%$search%' OR email LIKE '%$search%')";
    $result = $conn->query($query);

    while ($row = $result->fetch_assoc()) {

        echo "<li>{$row['name']} ({$row['company']}, {$row['number']}, {$row['email']})</li>";
    }

    exit(); 
}


$user_id = $_SESSION['user_id'];
$query = "SELECT * FROM contacts WHERE user_id = $user_id";
$result = $conn->query($query);

$items_per_page = 10;
$total_items = $result->num_rows;
$total_pages = ceil($total_items / $items_per_page);

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$offset = ($page - 1) * $items_per_page;

$query = "SELECT * FROM contacts WHERE user_id = $user_id LIMIT $offset, $items_per_page";
$result = $conn->query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
<h2>Contact Page</h2>
        <form id="searchForm">
            <input type="text" name="search" placeholder="Search">
            <input type="submit" value="Search">
            <a><button id="addContactButton">Add Contacts</button></a>
        </form>

        <table id="contactList">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>{$row['name']}</td>";
                    echo "<td>{$row['company']}</td>";
                    echo "<td>{$row['number']}</td>";
                    echo "<td>{$row['email']}</td>";
                    echo "<td><a href='edit_contact.php?id={$row['id']}'>Edit</a></td>";
                    echo "<td><button class='delete-btn' data-id='{$row['id']}'>Delete</button></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>

        <div id="pagination">
            <?php
            for ($i = 1; $i <= $total_pages; $i++) {
                
                echo "<a href='contacts.php?page=$i'>$i</a> ";
            }
            ?>
        </div>

        <div id="deleteModal" class="modal">
            <div class="modal-content">
                <p>Are you sure you want to delete this contact?</p>
                <button id="deleteYes">Yes</button>
                <button id="deleteNo">No</button>
            </div>
        </div>
    </body>
</html>

<script>
    $(document).ready(function () {
        $("#deleteModal").hide();

        $("#searchForm").submit(function (e) {

            e.preventDefault();
            var search = $("input[name='search']").val();
            loadContacts(search);
        });

        loadContacts('');

        $("#addContactButton").click(function() {

            window.location.href = 'add_contact.php';
        });

        function loadContacts(search) {

            $.ajax({
                type: "GET",
                url: "contacts.php",
                data: { search: search },
                success: function (response) {
                    // $("#contactList tbody").html(response);
                    //search should be working but i need to think of some better approach on this
                }
            });
        }
        

        $("#deleteYes").click(function () {
            var contactId = $("#deleteModal").data("contactId");

            $.post("delete_contact.php", { id: contactId }, function (data) {
            $("#deleteModal").hide();
            location.reload(true);
            console.log(data);
            });

            $("#deleteModal").hide();
        });

        $("#deleteNo").click(function () {
            $("#deleteModal").hide();
        });

        $("#contactList").on("click", ".delete-btn", function () {
            var contactId = $(this).data("id");
            console.log("Delete button clicked for contact ID: " + contactId);

            $("#deleteModal").data("contactId", contactId).show();
        });
    });
</script>

<style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h2 {
            color: #333;
        }

        #searchForm {
            margin-bottom: 10px;
        }

        #contactList {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        #contactList th, #contactList td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: left;
        }

        #contactList th {
            background-color: #f2f2f2;
        }

        #pagination {
            margin-top: 20px;
        }

        #pagination a {
            padding: 5px;
            text-decoration: none;
            color: #333;
            margin-right: 5px;
            border: 1px solid #ddd;
            background-color: #f2f2f2;
        }

        .modal {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            display: flex; /* Use flex container */
            justify-content: center;
            align-items: center;
        }

        .modal-content {
            background-color: #fefefe;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
        }

        #deleteYes,
        #deleteNo {
            margin-top: 10px;
            padding: 5px 10px;
            cursor: pointer;
        }

</style>
