<?php
session_start();
session_destroy();

// Login / Logout After logging in, redirect to contact page
header("Location: authentication.html");
exit();
?>
