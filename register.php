<?php
include 'db_config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name = $_POST["regName"];
    $email = $_POST["regEmail"];
    $password = $_POST["regPassword"];
    $confirmPassword = $_POST["confirmPassword"];

    $checkEmailQuery = "SELECT * FROM users WHERE email = '$email'";
    $result = $conn->query($checkEmailQuery);

    if ($result->num_rows > 0) {

        echo "email_exists";
    } else {

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $insertQuery = "INSERT INTO users (name, email, password) VALUES ('$name', '$email', '$hashedPassword')";
        
        if ($conn->query($insertQuery) === TRUE) {

            $query = "SELECT * FROM users WHERE email = '$email'";
            $resultCheck = $conn->query($query);

            $row = $resultCheck->fetch_assoc();
            $checkHashedPassword = $row['password'];
    
            if (password_verify($password, $checkHashedPassword)) {
                session_start();
                $_SESSION['user_email'] = $email;
                $_SESSION['user_id'] = $row['id'];  
            }

            echo "success";
            
        } else {
            echo "failure";
        }
    }
}

$conn->close();
?>
