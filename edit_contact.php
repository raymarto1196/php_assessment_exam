<?php
session_start();

if (!isset($_SESSION['user_email'])) {
    header("Location: login.php");
    exit();
}

include 'db_config.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];

    $user_id = $_SESSION['user_id'];
    $contact_id = $_POST['id'];

    $query = "UPDATE contacts SET name=?, company=?, email=?, number=? WHERE user_id=? AND id=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("ssssii", $name, $company, $email, $phone, $user_id, $contact_id);
    $result = $stmt->execute();

    if ($result) {
        header("Location: contacts.php");
        exit();
    } else {
        echo "Error updating contact: " . $stmt->error;
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id'])) {
    $contact_id = $_GET['id'];
    $user_id = $_SESSION['user_id'];

    $query = "SELECT * FROM contacts WHERE user_id = $user_id AND id = $contact_id";
    $result = $conn->query($query);

    if ($result->num_rows > 0) {
        $contact = $result->fetch_assoc();
    } else {
        header("Location: contacts.php");
        exit();
    }
} else {
    header("Location: contacts.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
    <h2>Edit Contact</h2>

    <form id="EditContactForm" method="POST">
        <?php if (isset($contact) && !empty($contact)) : ?>
            <input type="hidden" name="id" value="<?php echo $contact['id']; ?>">

            <label for="name">Name:</label>
            <input type="text" name="name" value="<?php echo $contact['name']; ?>" required><br>

            <label for="company">Company:</label>
            <input type="text" name="company" value="<?php echo $contact['company']; ?>" required><br>

            <label for="email">Email:</label>
            <input type="email" name="email" value="<?php echo $contact['email']; ?>" required><br>

            <label for="phone">Phone:</label>
            <input type="text" name="phone" value="<?php echo $contact['number']; ?>" required><br>

            <input type="submit" value="Submit">
        <?php else : ?>
            <p>Contact not found.</p>
        <?php endif; ?>
    </form>
</body>
</html>


<style>
    body {
        font-family: Arial, sans-serif;
        margin: 20px;
    }

    h2 {
        color: #333;
    }

    #EditContactForm {
        width: 50%;
        margin-top: 20px;
    }

    #EditContactForm label {
        display: block;
        margin-bottom: 5px;
    }

    #EditContactForm input {
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
</style>
